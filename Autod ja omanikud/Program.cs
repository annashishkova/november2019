﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace Autod_ja_omanikud
{
    static class E     // static - odin edinstvennyi klass, kotoryi delitsja mezdu vsemi, vse faily 4itajutsja iz odnogo klassa
    {
        public static string[] LoeFile(this string filename) => System.IO.File.ReadAllLines($"..\\..\\{filename}.txt");

        public static string[] Tükelda(this string rida) => rida.Split(',');

        public static string Küsi(this string küsimus)
        {
            Console.Write($"{küsimus}: ");
            return Console.ReadLine();
        }
    }
    
    
    class Program
    {
        static void Main(string[] args)
        {

            var autodF = "Autod".LoeFile();                 // var -  dlja ljuboi funkcii 
            //string[] autodF = "Autod".LoeFile();                 // var -  dlja ljuboi funkcii 
            var juhidF = "Juhid".LoeFile();
            var omanikudF = "Omanikud".LoeFile();
            // kolm faili sisse loetud

            Dictionary<string, Auto> autodD = new Dictionary<string, Auto>();
            Dictionary<string, Inimene> juhidD = new Dictionary<string, Inimene>();
            // nüüd on kus autosid ja inimesi pidada


           
            





            foreach (var x in autodF)
                {
                var xx = x.Tükelda();

                autodD.Add(xx[2], new Auto { Mark = xx[0], Mudel = xx[1], Numbrimärk = xx[2] });
            }
            
            foreach (var x in juhidF)
            {
                var xx = x.Tükelda();
                juhidD.Add(xx[1], new Inimene { Nimi = xx[0], IK = xx[1] });
            }

            // veel omanikud kirja panna

            foreach(var x in omanikudF)
            {
                var xx = x.Tükelda();

                var isikukood = xx[0];
                var autoNumber = xx[1];

                //var person = juhidD[isikukood];
                //var car = autodD[autoNumber];

                //person.Autod.Add(car);
                //car.Omanik = person;


                juhidD[isikukood].Autod.Add(autodD[autoNumber]);
                autodD[autoNumber].Omanik = juhidD[isikukood];

            }
            // kui nüüd veel midagi vaja teha, siis teeb keegi teine
            var märk = "Anna numbrimärk".Küsi();
            Console.WriteLine(
                autodD.ContainsKey(märk) ? autodD[märk].Omanik?.Nimi ?? "omanik puudub" : "sellist autot pole"
                );

            var kood = "Anna isikukood".Küsi();
            if (juhidD.ContainsKey(kood))
            {
                if (juhidD[kood].Autod.Count > 0)
                    foreach (var aa in juhidD[kood].Autod) Console.WriteLine($"{aa.Numbrimärk} {aa.Mark} { aa.Mudel}");

                else Console.WriteLine("autosid tal ei ole");
            }
            else Console.WriteLine("tundmatu isik");
            
        }
    }

    class Auto
    {
        public string Mark { get; set; }

        public string Mudel { get; set; }

        public string Numbrimärk { get; set; }

        public Inimene Omanik { get; set; }         // ustanavlivaju komu prinadlezit mashina, nazna4aju peremennuju Omanik
    }



    class Inimene
    {
        public string Nimi { get; set; }
        public string IK { get; set; }

        public List<Auto> Autod { get; set; } = new List<Auto>();  // List - eto tip peremennoi, kotoryi v sebe mozet soderzat tolko peremennuju tipa Auto, v dannom slu4ae class Auto
    
    
    
    
    }



}
