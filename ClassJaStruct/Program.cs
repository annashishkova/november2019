﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassJaStruct
{
    class Program
    {
        static void Main(string[] args)
        {
            InimeneClass esimeneC = new InimeneClass();
            esimeneC.Nimi = "Henn";
            esimeneC.Vanus = 64;
            InimeneClass teineC = esimeneC;
            teineC.Nimi = "Sarviktaat";
            Console.WriteLine(esimeneC.Nimi);
            Console.WriteLine(teineC.Nimi);

            InimeneStruct esimeneS = new InimeneStruct();
            esimeneS.Nimi = "Henn";
            esimeneS.Vanus = 64;
            InimeneStruct teineS = esimeneS;
            teineS.Nimi = "Sarviktaat";
            Console.WriteLine(esimeneS.Nimi);
            Console.WriteLine(teineS.Nimi);

            //int[] arvud = { 1, 2, 3, 4, 5 };

            // //int[] teised = arvud;

            //int[] teised = (int[])arvud.Clone();                // Klon pervogo massiva

            //teised[2] = 7;                                      // 0,1,2 - 2 eto tretii element massiva, kotoryi zamenjajut na cifru 7

            //Console.WriteLine($"{{{string.Join(",", arvud)}}}"); // troinye logi4eskie skobki, togda na konsoli vylezut skobki {} u massiva

        }
    }

    // ma teen Hästi lihtsa klassi, siis saame proovida üht asja

    struct InimeneStruct
    
   {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"Inimene (struct) {Nimi} ja vanusega {Vanus}";
    }

    class InimeneClass
    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"Inimene (class) {Nimi} ja vanusega {Vanus}";

    }



}

