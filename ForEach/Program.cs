﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForEach
{

    static class E
    {
        // siia ma hakkan oma funktsioone ehitama
        public static IEnumerable<int> Paaris(this IEnumerable<int> mass)       // funktsioon; this - ozna4aet, 4to mozno vyzvat funkciju iz ... .IEnumerable
        {
            foreach(var x in mass)
                if (x % 2 == 0) yield return x;
        }
        public static IEnumerable<int> Esimesed(this IEnumerable<int> mass, int mitu)
        {
            int mitmes = 0;
            foreach (var x in mass)
                if (mitmes++ < mitu) yield return x; else break;
        }

        public static IEnumerable<int> Paaritu(this IEnumerable<int> mass)       // funktsioon; this - ozna4aet, 4to mozno vyzvat funkciju iz ... .IEnumerable
        {
            foreach (var x in mass)
                if (x % 2 == 1) yield return x;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Func<int, int> ruut = x => x * x;
            Console.WriteLine( ruut(4));

            Func<int, bool> kasSuur = x => x > 5;

            if (kasSuur(ruut(4))) Console.WriteLine("on küll suur);


            // nüüd me õpime midagi ILUSAT!

            int[] arvud = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            foreach(var x in arvud
                //.Paaris()
                //.Esimesed(5)
                //.Paaritu()
                .Millised(x => x % 2 == 0)
                .Millised(x => x < 10)

                ) Console.WriteLine(x);

            foreach (var x in protok) Console.WriteLine(x);

            Console.WriteLine("kiireim oli: " +
                protok.OrderByDescending(x => x.Kiirus).First().Nimi
                );


           
        }
    }
}
