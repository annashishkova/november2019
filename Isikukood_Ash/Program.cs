﻿using System;

using System.Collections.Generic;

using System.Linq;

using System.Text;

using System.Threading.Tasks;



namespace ConsoleApp1

{



    class Program

    {

        static void Main(string[] args)

        {



            Person p1 = new Person(38512180295); // это создание и сохранение объекта в переменную

            Person p2 = new Person(48609110248);

            Person p3 = new Person(61601240125);

            Person p4 = new Person(61711010089);



            Console.WriteLine(p1.Gender);

            Console.WriteLine(p1.Age);

            Console.WriteLine(p1.Birthdate);

            Console.WriteLine(p1);

        }

    }




    class Person                    // класс описывает как будет выглядеть объект, свойства и тд

    {

        public static readonly string[] MaleGenderCharList = { "1", "3", "5" };

        public static readonly string[] FemaleGenderCharList = { "2", "4", "6" };



        public int Age;             // это атрибуты(аксессуары) класса

        public GenderEnum Gender;

        public DateTime Birthdate;



        public Person(long socialNumber)     // Конструктор - в нем мы создаем объект

        {

            string socialNumberString = socialNumber.ToString();             // ToString - это презентация объекта в стринговом формате -

                                                                             // это значит, что цифры превратятся в цифры в текстовом формате, в кавычках



            Gender = FigureOutGender(socialNumberString.Substring(0, 1));   // FigureOutGender(название) - это функция, которая выдает Gender типа enum

                                                                            // Substring(0,1) - 0 это индекс для первого символа, 1 это количество символов,

                                                                            // которые будут задействованы

            Birthdate = FigureOutBirtdate(socialNumberString);


            Age = DateTime.Today.Year - Birthdate.Year;


        }

        public static GenderEnum FigureOutGender(string genderChar)     // Описываем функцию, что она возвращает(GenderEnum)

        // и что она принимает (string genderChar)

        {

            if (MaleGenderCharList.Contains(genderChar))

            {
                return GenderEnum.Male;
            }

            else if (FemaleGenderCharList.Contains(genderChar))

            {
                return GenderEnum.Female;
            }

            else

            {
                throw new Exception("Couldn't figure out gender");
            }

        }

        public static DateTime FigureOutBirtdate(string socialNumber)

        {

            string centuryNumber = socialNumber.Substring(0, 1);

            int year = System.Convert.ToInt32(socialNumber.Substring(1, 2));

            int month = System.Convert.ToInt32(socialNumber.Substring(3, 2));

            int day = System.Convert.ToInt32(socialNumber.Substring(5, 2));





            // todo extract this to separate method

            int century;

            if (centuryNumber == "1" || centuryNumber == "2")

            {
                century = 1800;
            }

            else if (centuryNumber == "3" || centuryNumber == "4")

            {
                century = 1900;
            }

            else if (centuryNumber == "5" || centuryNumber == "6")

            {
                century = 2000;
            }

            else

            {
                throw new Exception("Couldn't figure out century");
            }



            return new DateTime(century + year, month, day);

        }



        public override string ToString()

        {
            return $"Gender:{Gender} | Age:{Age} | Birthdate: {Birthdate}";
        }



    }



    enum GenderEnum
    {           // enum - это создание своего типа, и мы задаем какие у него могут быть значения, в данном случае только Male, Female

        Male,

        Female,

    }



}