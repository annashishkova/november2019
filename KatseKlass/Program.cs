﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatseKlass
{
    class Program
    {
        static void Main(string[] args)
        {

          
            List<Person> koolipere = new List<Person>
            {
                new Student {Nimi = "Toomas Linnupoeg", Klass = "1A" };
                new Student { Nimi = "Kati Kaalikas", Klass = "1A" };

                new Teacher { Nimi = "Henn Sarv", Aine = "matemaatika õpetaja" };
                new Teacher { Nimi = "Henn Sarv", Aine = "matemaatika õpetaja" };

                new Person { Nimi = "direktor Kalju" };
            }
        };
    }

    class Person
    {
        public string Name;

        public override string ToString() => $"{Nimi}";
    }

    class Student : Person
    {
        public string Klass;

        public override string ToString() => $"{Klass} klassi õpilane {Nimi}";
    }


    class Teacher : Person
    {
        public string Aine;

        public override string ToString() => $"{Aine} õpetaja {Nimi}";

    }
}
