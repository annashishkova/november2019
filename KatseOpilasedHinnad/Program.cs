﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace KatseOpilasedHinnad
{

    class Program
    {
        static void Main(string[] args)
        {

            string filename = "..\\..\\Opilased.txt";
            
                var Opilased = System.IO.File.ReadAllLines(filename)
                .Select(x => x.Split(','))
                .Select(x => new { IK = x[0].Trim(), Nimi = x[1].Trim(), Klass = x[2].Trim() })
                .ToDictionary(x => x.IK);


            var list = Opilased.Values

                .GroupBy(x => x.Klass)
                ;

            foreach (var v in list)
            {
                Console.WriteLine($"Klassi {v.Key} nimekiri");
                foreach(var n in v)
                    Console.WriteLine($"\t{n.Nimi} - {n.IK}");
            }

            var Hinded = System.IO.File.ReadAllLines("..\\..\\Hinded.txt")
                .Select(x => x.Split(','))
                .Select(x => new { OpilaseIK = x[1].Trim(), OpetajaIK = x[0].Trim(), Aine = x[2].Trim(), Hinne = int.Parse(x[3])})
                ;
            foreach (var h in Hinded) Console.WriteLine($"Opilane {(Opilased.ContainsKey(h.OpilaseIK) ? Opilased[h.OpilaseIK].Nimi : h.OpilaseIK) } sai aines {h.Aine} hindeks {h.Hinne}");

            var OpilasteKeskmised = Hinded
                .GroupBy(x => x.OpilaseIK)
                .Select(x => new { Nimi = Opilased[x.Key].Nimi, Keskmine = x.Average(y => y.Hinne) });

            foreach (var v in OpilasteKeskmised) Console.WriteLine(v);

            // leia üles iga aine parim õpilane!


            var AineParimad = Hinded
                .GroupBy(x => x.Aine)
                .Select(x => new { Aine = x.Key, Parimad = x.OrderByDescending(y => y.Hinne).First() })
                .Select(x => )
                .Select(x => new { x.Aine, Parimad = Opilased[x.Parimad.OpilaseIK].Nimi, Hinne = x.ParimHinne });

            foreach (var v in AineParimad) Console.WriteLine(v);

            var Aineparimad = Hinded
                .GroupBy(x => x.Aine)
                .Select(x => new { Aine = x.Key, Parimad = x, ParimHinne = x.Max(y => y.Hinne) })
                .Select(x => new { x.Aine, Parimad = x.Parimad.Where(y => y.Hinne == x.ParimHinne) })
                ;

            foreach(var a in AineParimad)
                {
                Console.WriteLine("parimad õpilased aines: " + a.Aine);
                foreach(var v in a.Parimad)
                    Console.WriteLine($"\t{Opilased[v.OpilaseIk].Nimi} - hindega {v.Hinne}");
            }



        }
    }


}
