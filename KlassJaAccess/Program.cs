﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassJaAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene("35503070211") //, "henn")
            {
                Nimi = "henn",  // siin kasutatakse property set meetodit
                //IK = "35503070211"
            };

            henn.Nimi = "sarviktaat"; // siin samuti

            Console.WriteLine(henn.Nimi);
            Console.WriteLine(henn.DateOfBirth);
            Console.WriteLine(henn.Gender);
            Console.WriteLine(henn.Age);

  
        
        }
    }

    
}
