﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassJaMeetod
{
    class Program
    {
        static void Main(string[] args)
        {




            Inimene mina = new Inimene();

            mina.Nimi = "Anna";
            mina.Vanus = 33;
            Console.WriteLine(mina);


            Inimene teineMina = new Inimene() 
            { 
               Nimi = "Anna", 
               Vanus = 33 
            };

            Inimene laps = new Inimene { "Kalle", Vanus = 16 };

            MyyViina(laps);
            MyyViina(mina);
        }

        
    }

    class Inimene
    {
        public string Nimi;
        public int Vanus;
        public static int VanusePiir = 18;




        public override string ToString()
        {
            return $"Inimene {Nimi} vanusega {Vanus}";
        }

        public bool KasLaps()
        {
            return Vanus < VanusePiir;
        }

        static void MyyViina(Inimene x)
        {
            if (x.KasLaps()) Console.WriteLine($"{x.Nimi} viina ei saa");
            else Console.WriteLine($"No osta siis viina {x.Nimi}");
        }


    }
}


