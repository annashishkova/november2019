﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidAlgus
{
    class Inimene
    {
        public string Nimi;
        public int? Vanus;
        //public int KingaNumber; // TODO selle jaoks pean ma veel miskid reeglid välja mõtlema

        public override string ToString()
        {
            return $"Inimene {Nimi} vanusega {(Vanus?.ToString() ?? "mida me ei tea")}";
        }


        // tegemist on funktsiooniga
        public int? OletatavSünniaasta()
        {
            return DateTime.Now.Year - Vanus;
        }
    }
}
