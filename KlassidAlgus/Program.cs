﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//TODO: hiljem korjame siit ülearused read ära

namespace KlassidAlgus
{
    class Program
    {

        static void Main(string[] args)
        {
            //ÜtleTere();
            //ÜtleTere("Henn");
            //TervitaPikalt("Henn", "Sarv");

            Console.WriteLine("Kes sa oled: "); // dolzno byt v ;Main metode
            string kes = Console.ReadLine();
            Console.WriteLine(ToProper(kes));


            Inimene mina = new Inimene();
            mina.Nimi = "Anna";
            mina.Vanus = 33;

            Inimene tema = new Inimene();
            tema.Nimi = "Polina Shishkova";
            Console.WriteLine(tema.Vanus);

            Inimene[] inimesed = new Inimene[10];
            inimesed[0] = mina;
            inimesed[1] = tema;

            //Console.WriteLine(mina);
            //Console.WriteLine(tema);

            //mina.Trüki();


        }

        static void ÜtleTere() 
        {
            Console.WriteLine("Tere!");
        }


        static void ÜtleTere(string nimi)
        {
            Console.WriteLine($"Tere {nimi}!");
        }

        static void TervitaPikalt(string eesNimi, string pereNimi )
            {
                Console.WriteLine($"Tere {eesNimi}!");
                Console.WriteLine($"Mul on hea meel näha kedagi perekonnast Sarv");
            }

        
        
        
        
        
        static string ToProperx(string nimi)
        {


            //kuidas teha suure algustähega nimeks
            string esiTäht = nimi.Substring(0,1);
            string teisedTähed = nimi.Substring(1);


            return nimi = esiTäht.ToUpper() + teisedTähed.ToLower();

        }

        static string ToProper(string text)
        {
            string[] osad = text.Split(',');
            for (int i = 0; i < osad.Length; i++)
            {
                osad[i] = ToProperx(osad[i]);
            }
            return String.Join(" ", osad);


            // TODO ja siis veel nii, et sidekriipsuga nimed ka teisendatakse
            // TODO pille-riin sarv -> Pille-Riin Sarv
        }


        }
    }
   
