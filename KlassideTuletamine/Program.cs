﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom kroko = new Loom { Liik = "krokodill" };
            //kroko.TeeHäält();
            Loom ämmelgas = new Loom("ämblik");
            //ämmelgas.TeeHäält();
            Loom elajas = new Loom();
            //elajas.TeeHäält();

            //Koduloom k = new Koduloom {"kass", "Miisu" };
            //k.TeeHäält();

            Kass k = new Kass("Miisu", "angora");
            //k.TeeHäält();

            List<Loom> loomad = new List<Loom>
            {
                kroko,
                ämmelgas,
                elajas,
                k,
                new Koduloom ("lammas", "Dolly")
            };

            foreach (var x in loomad) x.TeeHäält();
            foreach (var x in loomad) Console.WriteLine(x);
            foreach (var x in loomad) x.SikutaSabast();

            k.Silita();
            k.TeeHäält();
            k.SikutaSabast();
            k.TeeHäält();

        }
    }


    class Loom
    {
        public string Liik;
        public Loom(string liik) => Liik = liik;

        public Loom() : this("tundmatu") { }        // this vyzyvaet predydushii konstructor

        public virtual void TeeHäält() => Console.WriteLine($"{Liik} teeb koledat häält");

        public override string ToString() => $"loom liigist {Liik}";

        public override string ToString()
        {
            return "kodu" + base.ToString();
        }

        public void SikutaSabast() => $"HOIATUS kui sikutada {Liik} looma sabast, siis see ei ole hea";
    }


    class Koduloom : Loom
    {
        public string Nimi;

        public Koduloom(string liik, string nimi) : base(liik) => Nimi = nimi;

        public override void TeeHäält() => Console.WriteLine($"{Nimi} möriseb mõnusasti");
        

    }

    class Kass : Koduloom
    {
        public string Tõug;
        public bool Tuju = false;

        public Kass(string nimi, string tõug) : base("Kass", nimi) => Tõug = tõug;     // konstruntor

        public void Silita() => Tuju = true;
        public new void SikutaSabast() => Tuju = false;

        public override void TeeHäält()
        {
            if (Tuju) Console.WriteLine($"Kass {Nimi} lööb nurru");
            else Console.WriteLine($"{Nimi} kräunub");
        }

        class Ahaa

    }

}


