﻿using System;
using System.Collections.Generic;


namespace Reede
{
    class Program
    {
        static void Main(string[] args)
        {
            // muutujad ja andmetüübid on selged
            // muutujal on neli asja (nimi, tõõp, väärtus, skoop)

            DateTime täna = DateTime.Today;
            DateTime anna = new DateTime(1986, 9, 11);
            
            var vahe = täna - anna;
            Console.WriteLine(vahe.Days);                   // anna elatud päevade arv
            Console.WriteLine(vahe.Days*4/1461);            // anna elatud täisaastaid

            Console.WriteLine(täna.Year - anna.Year);    // anna sünniaasta ja tänavuse aasta vahe

            // vanus kuudes

           switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Friday:
                case DayOfWeek.Tuesday:
                    // reedesed toimingud
                    break;

                case DayOfWeek.Sunday:
                    // laupäevane ja pühapäevane
                    break;

                case DayOfWeek.Saturday:
                    // laupäevane toiming
                    goto case DayOfWeek.Sunday;

                case DayOfWeek.Monday:
                    // esmaspäevased toimingud
                    break;

                default:
                    // kõigi muude päevade toimingud
                    break;
            }

            //int[] arvud = new int[10]; // see on muutuja, mis sisaldab 10 int-i (vaikimisi kõik nullid)

            //int[] arvud2 = new int[5] { 7, 3, 2, 9, 6 };  // see on muutuja, mis sisaldab 5 arvu - 7, 3, 2, 9, 6

            //int[] arvud3 = { 1, 2, 7 }; // see on muutuja, mis sisaldab 3 arvu - 1, 2, 7
            //string[] nimed =
            //{
            //    "Henn",
            //    "Ants",
            //    "Peeter",
            //    "Joosep",
            //    "Malle",
            //    "Ülle"

            //};  // See on stringide massiiv, kus on 6 kohta ja igal kohal mingi väärtus

            //var inimene = new { Nimi = "Henn", vanus = 64 };

            //Dictionary<string, int> vanused = new Dictionary<string, int>

            //{
            //    {"Henn",65 },
            //    {"Anna",33 },
            //    {"Darja",34 },
            //    {"Asja",2 },
            //};
            //vanused.Add("Malle", 22);

            for (int rebane = 0; rebane < 10; rebane++)
            {
            Console.WriteLine($"rebane hüppab {rebane+1}. korda");
            }

            string testSplit = "Henn on ilus poiss";    // string muutuja (või avaldis)

            string[] splititud = testSplit  
                 .Split(' ');                                       // kui selle "otsa" kirjutada Split-funktsioon
                                                                    // tulemuseks stringimassiiv
            splititud[0] = "Ants";

            testSplit = string.Join(" ", splititud);    // see paneb tükeldatud "massiivi" tagasi stringiks
            Console.WriteLine(testSplit);


            // int.Parse("4723")            // -> teisendab TEKSTI arvuks int 4723              kaval, tark ja viisakas progeja
            // Convert.ToInt32("4723")      // teeb TÄPSELT sama                                laisk, googli-usku, mujalt keelest tulnu

            Console.Write("Nimi ja palk palun:" );
            palk = int.Parse(Console.ReadLine());                        //loeb ekraanilt teksti ja teisendab

            if (int.TryParse(
                Console.ReadLine(),
                out palk)

                { } else { Console.WriteLine("ise oled"); }));







        }
    }
}
