﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReedeLisaks
{

    
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Massiiv, mis koosneb 52 elementidest");

            int[] numbers = new int[52];

            int[,] myArr = new int[13, 4];
            Random ran = new Random();

            for (int i = 0; i < 13; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    myArr[i, j] = ran.Next(1, 52);
                    Console.Write("{0}\t", myArr[i, j]);
                }
                Console.WriteLine();
            }



            Console.WriteLine("massiiv 10x10");

            int[] numbers1 = new int[100];

            int[,] myArr1 = new int[10, 10];
            Random ran1 = new Random();

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    myArr1[i, j] = ran1.Next(1, 100);
                    Console.Write("{0}\t", myArr1[i, j]);
                }
                Console.WriteLine();
            }


            Console.Write("Mida otsime");
            int otsime = int.Parse(Console.ReadLine());

            for (int i =0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (myArr1[i, j] == otsime) Console.WriteLine($"leidsin reast {i} veerust {j}");
                }
            }


            
        }
    }
}
