﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace Spordipäev
{
    class Program
    {
        static void Main()
        {

            string filename = @"..\..\spordipäeva protokoll.txt";               // "..\\..\\spordipäeva protokoll.txt"; - tak toze mozno zapisat otkuda brat fail txt
            string[] tulemusedtxt = System.IO.File.ReadAllLines(filename);
            List<Tulemus> tulemused = new List<Tulemus>();                      // nuzno sdelat List s rezultatami
            for (int i = 0; i < tulemusedtxt.Length; i++)                       // esli napisat int i = 1, to eto ozna4aet, 4to my propuskaem pervyi rjad i na4inaes s4ityvat info, na4inaja so vtorogo rjada
            {
                string[] reaosad = tulemusedtxt[i].Split(',');
                tulemused.Add(
                    new Tulemus
                    {
                        Nimi = reaosad[0],
                        Distants = double.TryParse(reaosad[1], out double d) ? d : 0,
                        Aeg = double.TryParse(reaosad[2], out double a) ? a : 0,
                    }
                    );
            }
            foreach (var x in tulemused) Console.WriteLine($"{x.Nimi} \tjooksis {x.Distants} \tkiirusega {x.Kiirus:F2}");   // foreach (var x in tulemused.OrderByDescending(x=>x.Kiirus)) - na4net s4itat ot maksimalnobolshogo 4isla k menshemu

            // ma tahaks leida kiireima

            int kiireim = 0;

            for (int i = 0; i < tulemused.Count; i++)
            {
                if (tulemused[i].Kiirus > tulemused[kiireim].Kiirus) kiireim = i;
            }

            Console.WriteLine($"Kiireim oli {tulemused[kiireim].Nimi}");
        }
    }



    class Tulemus
    {

        public string Nimi { get; set; }

        public double Distants { get; set; }

        public double Aeg { get; set; }

        public double Kiirus => Distants / Aeg;

       
    }
}
