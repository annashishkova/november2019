﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace TextJaJson
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "..\\..\\spordipäeva protokoll.txt";
            string filenameJson = "..\\..\\protokoll.json";
            if (false)
            {
                var protok = System.IO.File.ReadAllLines(filename)
                       .Skip(1)
                       .Select(x => x.Split(','))
                       .Where(x => x.Length > 2)
                       .Select(x => new Tulemus { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2]), Kiirus = double.Parse(x[1]) / int.Parse(x[2]) })
                       //.Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2])})
                       //.Select(x => new { x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants * 1.0 / x.Aeg})
                       .ToList()
                       ;
                string json = 
                JsonConvert.SerializeObject(protok);
                File.WriteAllText(filenameJson, json);

            }
            string loeJson = File.ReadAllText(filenameJson);
            List<Tulemus> tulemused = JsonConvert.DeserializeObject<List<Tulemus>>(loeJson);

            foreach(var x in tulemused)
            {
                Console.WriteLine($"{x.Nimi} jooksis {x.Distants} ajaga {x.Aeg}");
            }



            string student = "..\\..\\Opilased.txt";
            string studentJson = "..\\..\\Opilased.json";

            if (false)
            {
                var  = System.IO.File.ReadAllLines(filename)
                       .Skip(1)
                       .Select(x => x.Split(','))
                       .Where(x => x.Length > 2)
                       .Select(x => new Tulemus { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2]), Kiirus = double.Parse(x[1]) / int.Parse(x[2]) })
                       //.Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = int.Parse(x[2])})
                       //.Select(x => new { x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants * 1.0 / x.Aeg})
                       .ToList()
                       ;
                string json =
                JsonConvert.SerializeObject(protok);
                File.WriteAllText(filenameJson, json);

            }
            string loeJson = File.ReadAllText(filenameJson);
            List<Tulemus> tulemused = JsonConvert.DeserializeObject<List<Tulemus>>(loeJson);

            foreach (var x in tulemused)
            {
                Console.WriteLine($"{x.Nimi} jooksis {x.Distants} ajaga {x.Aeg}");
            }


        }


    }

    public class Tulemus
    {
        public string Nimi { get; set; }
        public int Distants { get; set; }
        public int Aeg { get; set; }
        public double Kiirus { get => (double)Distants / Aeg; set { } }

    }

}
